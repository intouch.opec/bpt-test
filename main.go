/*
Write function `TwoSumExists` that takes in a list of integer and a target value.
The function returns true iff there exists two elements in the list that sum to target.
  Example:
    TwoSumExists([8, 20, 1, 15, 2],  9) == True (because 8 + 1 = 9)
    TwoSumExists([8, 20, 1, 15, 2], 22) == True (because 20 + 2 = 22)
    TwoSumExists([8, 20, 1, 15, 2], 18) == False
    TwoSumExists([8, 20, 1, 15, 2],  4) == False
    TwoSumExists([8, 20, 1, 2, 2],  4) == True
*/
// To execute Go code, please declare a func main() in a package "main"

package main

import "fmt"

func TwoSumExists(nums []int, target int) bool {
	lookup := map[int]int{}
	for index, num := range nums {
		if intInSlice(target-num, lookup) {
			return true
		}
		lookup[num] = index
	}
	return false
}

func intInSlice(a int, list map[int]int) bool {
	for i, _ := range list {
		if i == a {
			return true
		}
	}
	return false
}

func main() {
	fmt.Println(TwoSumExists([]int{8, 20, 1, 15, 2}, 9))
	fmt.Println(TwoSumExists([]int{8, 20, 1, 15, 2}, 22))
	fmt.Println(TwoSumExists([]int{8, 20, 1, 15, 2}, 18))
	fmt.Println(TwoSumExists([]int{8, 20, 1, 15, 2}, 4))
	fmt.Println(TwoSumExists([]int{8, 20, 1, 2, 2}, 4))
}
