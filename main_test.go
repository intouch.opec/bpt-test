package main

import "testing"

func benchmarkTwoSumExists(nums []int, target int, b *testing.B) {
	for n := 0; n < b.N; n++ {
		TwoSumExists(nums, target)
	}
}

func BenchmarkTwoSumExists1(b *testing.B) { benchmarkTwoSumExists([]int{8, 20, 1, 15, 2}, 9, b) }
func BenchmarkTwoSumExists2(b *testing.B) { benchmarkTwoSumExists([]int{8, 20, 1, 15, 2}, 22, b) }
func BenchmarkTwoSumExists3(b *testing.B) { benchmarkTwoSumExists([]int{8, 20, 1, 15, 2}, 18, b) }
func BenchmarkTwoSumExists4(b *testing.B) { benchmarkTwoSumExists([]int{8, 20, 1, 15, 2}, 4, b) }
func BenchmarkTwoSumExists5(b *testing.B) { benchmarkTwoSumExists([]int{8, 20, 1, 2, 2}, 4, b) }
